\documentclass{article}

\usepackage{titlesec}
\usepackage{titling}
\usepackage[margin=0.9in]{geometry}
\usepackage{enumitem}
\usepackage{multicol}
\setlength{\columnsep}{1cm}

\titleformat{\section}
{\Large\bfseries}
{}
{0cm}
{}[\titlerule]

\titleformat{\subsection}
{\bfseries}
{$\bullet$}
{0.25cm}
{}

\titleformat{\subsubsection}[runin]
{\bfseries}
{}
{0cm}
{}

\renewcommand{\maketitle}{
\begin{center}
{\huge\bfseries
\theauthor}\\
\vspace{0.25cm}
{\large Assistant Professor}\\
http://gmansell.gitlab.io/\\
glmansel@syr.edu\\
\end{center}
}

\begin{document}
\title{CV}
\author{Georgia Mansell}

\maketitle
\begin{minipage}{0.3\linewidth}
\textbf{Business address}\\
259 Physics Bldg\\
Syracuse University\\
140 Sims Drive \\
Syracuse, NY 13210

\end{minipage}
\begin{minipage}{0.7\linewidth}
\textbf{About me}\\
I am an assistant professor at Syracuse University.
My research interests include gravitational-wave detection, interferometry,
quantum optics, lasers, and astrophysics.
I have extensive lab experience with optics and optical cavities, fiber optics, electronics, and control systems.
I am proficient at data analysis and visualization with python and Matlab.
\end{minipage}

\titlespacing{\subsection}{0cm}{0.25cm}{0cm}

\section{Research Experience, Employment, and Education}
\subsection{Massachusetts Institute of Technology and LIGO Hanford Observatory, Postdoctoral Fellow}
\subsubsection{2018 -- Present}
Commissioning advanced LIGO detector for observing runs 3 and 4.
Understanding and budgeting limiting noise sources, lock acquisition and preparing the detector for observation.\\
Work on active mode matching, thermal compensation, arm length stabilization, and squeezer subsystems of the interferometer.
Some installation work in vacuum chambers and cleanroom environments.
Python scripting and working with test and measure equipment.
Research and development for a new active mode-matching elements.\\
Supervisors: Prof Nergis Mavalava, Dr Peter Fritschel, Dr Michael Landry, Dr Daniel Sigg

\subsection{Australian National University, PhD}
\subsubsection{2013 -- 2018}
PhD at the Research School of Physics and Engineering, thesis titled
``Vacuum compatible squeezed light sources for advanced gravitational-wave detectors".\\
Research focused on quantum optics, squeezed light generation, cavity design.
Devised and constructed a tabletop squeezed light source at a novel wavelength (1984 nm).\\
Supervisors: Prof David McClelland, Prof Daniel Shaddock, Dr Bram Slagmolen

\subsection{Massachusetts Institute of Technology, Visiting Student}
\subsubsection{Feb -- Oct 2015}
Built and characterized a low phase noise, vacuum compatible squeezed light source as part of PhD research.
Acquired skills in optical cavity design and characterization, clean and bake procedures, electronics.\\
Supervisors: Dr Lisa Barsotti, Dr John Miller, Prof Nergis Mavalvala

\subsection{The University of Adelaide, BSc (hons)}
\subsubsection{2009 -- 2012}
Honours dissertation titled ``Stimulated Brillouin scattering mitigation in fiber lasers"\\
Supervisors: Prof Jesper Munch, Dr David Lancaster

\section{Awards}
\begin{itemize}[leftmargin=*]
\item Syracuse University Physics Department Large Service Course Teaching Award Fall 2023
\item Ozgrav travel award (2017)
\item John Carver Seminar Series Director's Choice (2016)
\item Robert and Helen Crompton Travel Award (2014)
\item Australian postgraduate award (2014)
\item Australian National University Supplementary Scholarhsip (2013)
\item Ferry Scholarship in honours Chemistry and Physics (2012)
\item DSTO Scholarship in photonics (2009, 2010, 2011)
\end{itemize}
\pagebreak
\section{Teaching experience}
\subsection{Syracuse University Physics 101 - Major Concepts in Physics 1}
\subsubsection{Fall 2023, Fall 2024}

\subsection{Syracuse University Physics 317 - Stellar and Interstellar Astrophysics}
\subsubsection{Spring 2024, Spring 2025}

\subsection{National Youth Science Forum and Australian National University}
\subsubsection{Feb 2016}
Lab demonstrator and instructor for interferometry and spectroscopy experiments for high school students

\subsection{Australian National University}
\subsubsection{2013 -- 2015}
Lab demonstrator and instructor for fiber optic communications course for third year undergraduate students.

\section{Key Presentations}

\subsection{October 2024 -- Binghamton University Physics Colloquium (Binghamton, NY)}
Detecting gravitational waves: from instrumentation to compact binariesß

\subsection{May 2024 -- Gravitational-Wave Advanced Detector Workshop (Hamilton Island, Australia)}
Quantum Noise \& Squeezing Tutorial

\subsection{April 2023 -- APS April Meeting (Minneapolis, MN)}
Invited talk: Status of the LIGO, Virgo, and KAGRA gravitational-wave detectors

\subsection{Nov 2020 -- IEEE Nuclear Science Symposium (remote)}
Plenary: ``The Advanced LIGO detections in the third observing run"

\subsection{Sep 2019 -- LIGO-Virgo-Kagra collaboration meeting (Warsaw, Poland)}
Keynote: ``LIGO Commissioning Update"

\subsection{Apr 2019 -- University of Eastern Washington (Cheney, WA)}
Physics colloquium: ``Status of the Advanced LIGO detectors"

\subsection{Dec 2018 -- Ozgrav retreat (Perth, Western Australia)}
Invited talk: ``Commissioning update"

\subsection{Dec 2018 -- Optomechanics incubator (Perth, Western Australia)}
Invited talk: ``Tackling quantum noise in near-term future gravitational-wave detectors"

\subsection{Jul 2018 -- Museum of Flight summer scholars program (Seattle, WA)}
Outreach talk: ``Black Holes, Neutron Stars, Lasers, and Gravitational Waves"

\subsection{May 2018 -- Gravitational-wave advanced detector workshop (GWADW) (Alaska)}
Contributed talk: ``2 um squeezing"

\subsection{Apr 2017 -- University of Queensland (Brisbane, Australia)}
Physics Colloquium: ``Squeezed light and gravitational wave detectors"

\subsection{Dec 2016 -- Australian Institute of Physics congress (Brisbane, Australia)}
Keynote: ``Development of a 2 um squeezed light source"

\subsection{Oct 2016 -- MIT General Relativity Informal Tea-Time Series (GRITTS) (Cambridge, MA)}
Seminar: ``An in-vacuum squeezed light source with in-vacuum homodyne detection"

\subsection{Mar 2015 -- LIGO Virgo collaboration meeting (Pasadena, CA)}
Contributed talk: ``Squeezed light generation using an in-vacuum OPO"

Invited talk: ``Basics of squeezing tutorial"

\pagebreak

\section{Publications}
\begin{itemize}[leftmargin=*]

\item E. Capote et al (LIGO O4 detector collaboration)
``Sensitivity and performance of the advanced LIGO detectors in the fourth observing run''
Under review. https://arxiv.org/abs/2411.14607 (2024)

\item W. Jia et al (LIGO O4 detector collaboration)
``Squeezing the quantum noise of a gravitational-wave detector below the standard quantum limit''
\textit{Science} 385, 1318-1321 (2024)

\item D. Ganapathy et al (LIGO O4 detector collaboration)
``Broadband Quantum Enhancement of the LIGO Detectors with Frequency-Dependent Squeezing''
Phys. Rev. X 13 041021 (2023)

\item S. Dwyer, G. Mansell, L. McCuller
``Review: Squeezing in Gravitational Wave Detectors''
Galaxies 10, 46 (2022)

\item C. Cahillane, G. Mansell
``Review of the Advanced LIGO Gravitational Wave Observatories Leading to Observing Run Four''
Galaxies 10, 36 (2022)

\item V. Srivastava, \textbf{G. Mansell}, W. Jia, et al
``Piezo-deformable mirrors for active mode matching in advanced LIGO'',
Opt. Express 30, 10491-10501 (2022) 

\item C. Cahillane, \textbf{G. Mansell}, D. Sigg,
``Laser frequency noise requirements for next generation gravitational-wave detectors'',
Opt. Express 29, 42144-42161 (2021) 

\item A. Buikema, C. Cahillane, \textbf{G. Mansell}, C. Blair, LIGO instrument scientists,
``Sensitivity and Performance of the Advanced LIGO Detectors in the Third Observing Run",
Phys. Rev. D 102, 062003 (2020)

\item M. Yap, J. Cripe, \textbf{G. Mansell}, T. McRae, R. Ward, B. Slagmolen, P. Heu, D. Follman, G. Cole, T. Corbitt, D. McClelland ,
``Broadband reduction of quantum radiation pressure noise via squeezed light injection",
Nature Photonics 14, pages 19 - 23 (2020)

\item S. Biscans, S, Gras, C. Blair, J. Driggers, M. Evans, P. Fritschel, T. Hardwick, \textbf{G. Mansell},
``Suppressing parametric instabilities in LIGO using low-noise acoustic mode dampers",
Phys. Rev. D 100, 122003 (2019)

\item M. Yap, D. Gould, T. McRae, P. Altin, N. Kijbunchoo, \textbf{G. Mansell}, R. Ward, D. Shaddock, B. Slagmolen, D. McClelland,
``Squeezed vacuum phase control at 2um",
Opt. Lett. 44, 21, pages 5386-5389 (2019)

\item \textbf{G. Mansell}, T. McRae, P. Altin, M. Yap, R. Ward, B. Slagmolen, D. Shaddock, and D. McClelland,
``Observation of Squeezed Light in the 2 um Region",
Phys. Rev. Lett. 120, 203603 (2018)

\item E. Oelker, \textbf{G. Mansell}, M. Tse, et al.,
``Ultra-low phase noise squeezed vacuum source for gravitational wave detectors",
Optica 3, 682-685 (2016)

\item A. Wade, \textbf{G. Mansell}, T. McRae, S. Chua, M. Yap, R. Ward, B. Slagmolen, D. Shaddock, D.
McClelland,
``Optomechanical design and construction of a vacuum-compatible optical
parametric oscillator for generation of squeezed light",
Review of Scientific Instruments 87, 6 (2016)

\item A. Wade, \textbf{G. Mansell}, S. Chua, R. Ward, B. Slagmolen, D. Shaddock, D. McClelland,
``A squeezed light source operated under high vacuum", Scientific Reports 5 (2015) 18052
\end{itemize}

\section{Grants}
\begin{itemize}[leftmargin=*]
    \item CAREER: Quantum Optics for Future Gravitational-Wave Detectors (2024 -- 2029)
    \item Active Violin Mode Dampers for Gravitational-Wave Detector Suspension Fibers (2024 -- 2027)
    \item From Detector Hardware to Astrophysics: an Open Control and Analysis Architecutre for Cosmic Explorer (2024 -- 2027)
    \item The CSUF-led Partnership for Inclusion of Underrepresented Groups in Gravitational-Wave Astronomy (2022 -- 2025)
    \item Collaborative Research: Cosmic Explorer Optical Design (2023 -- 2026)
\end{itemize}

\end{document}
